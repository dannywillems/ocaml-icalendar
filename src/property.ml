module type PROPERTY =
  sig
    (* Define a raw property *)
    type t

    (* The type of the value *)
    type value

    val to_str : t -> string
  end

module Raw : PROPERTY =
  struct
    type t =
    {
      name: string;
      params: Parameter.Raw.t list
    }

    type value = string ref

    let to_str t =
      t.name ^ (Parameter.param_list_to_str t.params (Parameter.Raw.to_str))
  end
