module type PROPERTY =
  sig
    (* Define a raw property *)
    type t

    (* The type of the value *)
    type value

    val to_str : t -> string
  end

module Raw : PROPERTY
