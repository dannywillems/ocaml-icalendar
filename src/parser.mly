%token BEGIN_VCALENDAR
%token <string> VERSION
%token <string> PRODID
%token <string> CALSCALE
%token <string> METHOD
%token END_VCALENDAR
%token EOF

%start <Icalendar.Vcalendar.t option> prog
%%

prog:
  | EOF { None }
  | v = vcalendar { Some v }
  ;

vcalendar:
  | BEGIN_VCALENDAR ; s = content ; END_VCALENDAR
  { let ical = Icalendar.Vcalendar.create () in s }

content:
  | s = VERSION { Icalendar.Vcalendar.set_version ical s }
  | s = PRODID { Icalendar.Vcalendar.set_prodid ical s }
  | s = CALSCALE { Icalendar.Vcalendar.set_calscale ical s }
  | s = METHOD { Icalendar.Vcalendar.set_method ical s }
  ;
