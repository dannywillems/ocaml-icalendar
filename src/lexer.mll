{
  open Lexing
  open Parser

  exception SyntaxError of string
  let next_line lexbuf =
    let pos = lexbuf.lex_curr_p in
    lexbuf.lex_curr_p <-
      {
        pos with  pos_bol = lexbuf.lex_curr_pos;
                  pos_lnum = pos.pos_lnum + 1
      }
}

(* General regex *)
let word = ['a'-'z' 'A'-'Z']
let colon = ':'
let content = word+
let white = ' '
let newline = '\n'

(* vcalendar regex *)
let begin_vcalendar = "BEGIN:VCALENDAR"
let end_vcalendar = "END:VCALENDAR"
let version = "VERSION" colon word+
let prodid = "PRODID" colon word+
let calscale = "CALSCALE" colon word+
let method_ = "METHOD" colon word+

rule read =
  parse
  | white { read lexbuf }
  | newline { next_line lexbuf ; read lexbuf }
  | begin_vcalendar { BEGIN_VCALENDAR }
  | end_vcalendar { END_VCALENDAR }
  | version { VERSION (Lexeme.lexeme lexbuf) }
  | prodid { PRODID (Lexeme.lexeme lexbuf) }
  | calscale { CALSCALE (Lexeme.lexeme lexbuf) }
  | method_ { METHOD (Lexeme.lexeme lexbuf) }
  | eof { EOF }
