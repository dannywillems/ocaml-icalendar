module Raw :
  sig
    type t =
    {
      name: string;
      mutable value: string
    }

    val create : string -> string -> t
    val to_str : t -> string
  end

module Value :
  sig
    type t =
    | Binary
    | Boolean
    | Cal_adress
    | Date
    | Date_time
    | Duration
    | Float
    | Integer
    | Period
    | Recur
    | Text
    | Time
    | Uri
    | Utc_offset
    | X_name of string
    | Iana_token of string

    val to_str : t -> string
  end

module Alternate_representation :
  sig
    type t = string

    val create : string -> t

    val to_str : t -> string
  end

module Common_name :
  sig
    type t = string

    val create : string -> t

    val to_str : t -> string
  end
(**
 * Convert a list of parameters in string.
 * The first parameter is a list of M.t type where M has the signature
 * PARAMETER.
 * The second parameter is the M.to_str function.
 *
 * FIXME: Property parameters with values containing a COLON character, a
 * SEMICOLON character or a COMMA character MUST be placed in quoted text.
 *)
val param_list_to_str : 'a list -> ('a -> string) -> string
