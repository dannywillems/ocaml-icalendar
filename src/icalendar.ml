(* ************************************************************************** *)
(* Standard components implementation *)
module type COMPONENT =
  sig
    type t
    val create : unit -> t

    (*
    val set_dtstart : t -> Date.t -> unit
    val set_dtend : t -> Date.t -> unit
    *)
    val set_summary : t -> string -> unit

    (*
    val dtstart : t -> Date.t
    val dtend : t -> Date.t
    *)
    val summary : t -> string

    val to_str : string
  end

(**
 * The Vevent component
 *)
module Vevent =
  struct
    type t =
    {
      mutable summary : string;
    }
    let create () = { summary = "" }
    let set_summary t s = t.summary <- s
    let summary t = t.summary
    let to_str = ""
  end

module Valarm =
  struct
    type t =
    {
      mutable summary : string;
    }
    let create () = { summary = "" }
    let set_summary t s = t.summary <- s
    let summary t = t.summary
    let to_str = ""
  end

module Vtodo =
  struct
    type t =
    {
      mutable summary : string;
    }
    let create () = { summary = "" }
    let set_summary t s = t.summary <- s
    let summary t = t.summary
    let to_str = ""
  end

type component =
  | Event of Vevent.t
  | Todo of Vtodo.t
  | Alarm of Valarm.t

module Vcalendar =
  struct
    type t =
    {
      mutable version : string;
      mutable prodid : string;
      mutable calscale : string;
      mutable method_ : string;
      (*
      components: component array
      *)
    }

    let create () =
      {
        version = "";
        prodid = "";
        calscale = "";
        method_ = "";
        (*
        components = []
        *)
      }

    let set_version t s = t.version <- s
    let set_prodid t s = t.prodid <- s
    let set_calscale t s = t.calscale <- s
    let set_method t s = t.method_ <- s
    (* let add_component t c = *)

    let version t = t.version
    let prodid t = t.prodid
    let calscale t = t.calscale
    let method_ t = t.method_
    (* let components t = t.components *)

    let to_str x = ""
  end


