(* ************************************************************************** *)
(**
 * Represent a component a vcalendar object can contain
 *)
module type COMPONENT =
  sig
    type t
    val create : unit -> t

    (*
    val set_dtstart : t -> Date.t -> unit
    val set_dtend : t -> Date.t -> unit
    *)
    val set_summary : t -> string -> unit

    (*
    val dtstart : t -> Date.t
    val dtend : t -> Date.t
    *)
    val summary : t -> string

    val to_str : string
  end

(**
 * The vevent component, including the raw component interface
 *)
module Vevent :
  sig
    include COMPONENT
  end

module Vtodo :
  sig
    include COMPONENT
  end

module Valarm:
  sig
    include COMPONENT
  end
(* ************************************************************************** *)

(* ************************************************************************** *)
type component

module Vcalendar :
  sig
    type t
    val create : unit -> t

    val set_version : t -> string -> unit
    val set_prodid : t -> string -> unit
    val set_calscale : t -> string -> unit
    val set_method : t -> string -> unit
    (* val add_component : t -> component -> unit *)

    val version : t -> string
    val prodid : t -> string
    val calscale : t -> string
    val method_ : t -> string
    (* val components : t -> component list *)

    val to_str : t -> string
  end
(* ************************************************************************** *)
