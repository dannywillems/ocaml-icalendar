(** {1 Raw property *)
module Raw =
  struct
    type t =
    {
      name: string;
      mutable value: string
    }

    let create n v = {name = n ; value = v}

    let to_str t = t.name ^ "=" ^ t.value
  end

(** {2 Value property *)
module Value =
  struct
    type t =
    | Binary
    | Boolean
    | Cal_adress
    | Date
    | Date_time
    | Duration
    | Float
    | Integer
    | Period
    | Recur
    | Text
    | Time
    | Uri
    | Utc_offset
    | X_name of string
    | Iana_token of string

    let to_str t = "VALUE=" ^ (match t with
    | Binary -> "BINARY"
    | Boolean -> "BOOLEAN"
    | Cal_adress -> "CAL-ADDRESS"
    | Date -> "DATE"
    | Date_time -> "DATE-TIME"
    | Duration -> "DURATION"
    | Float -> "FLOAT"
    | Integer -> "INTEGER"
    | Period -> "PERIOD"
    | Recur -> "RECUR"
    | Text -> "TEXT"
    | Time -> "TIME"
    | Uri -> "URI"
    | Utc_offset -> "UTC_OFFSET"
    | X_name x -> x
    | Iana_token x -> x)
  end

(** {3 Alternate representation} *)
(** To specify an alternate text representation for the property value.
 *
 * Full description: Page 15-16
 *)
module Alternate_representation =
  struct
    type t = string

    let create s : t = s

    (* The URI parameter value MUST be specified in a quoted-string. *)
    let to_str t = "ALTREP=\"" ^ t ^ "\""
  end

(** {4 Common name} *)
(**
 * To specify the common name to be associated with the calendar user specified
 * by the property.
 *
 * Full description: Page 16
 **)
module Common_name =
  struct
    type t = string

    let create s : t = s

    let to_str t = "CN=" ^ t
  end


(**
 * Convert a list of parameters in string.
 * The first parameter is a list of M.t type where M has the signature
 * PARAMETER.
 * The second parameter is the M.to_str function.
 *
 * FIXME: Property parameters with values containing a COLON character, a
 * SEMICOLON character or a COMMA character MUST be placed in quoted text.
 * Placed in quoted by default ?
 *)
let rec param_list_to_str l f = match l with
  | [] -> ":"
  | [x] -> ";" ^ f x ^ ":"
  | head::a -> ";" ^ (f head) ^ (param_list_to_str a f)
