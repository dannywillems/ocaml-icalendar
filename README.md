iCalendar library for OCaml
===========================

The [iCalendar](http://www.ietf.org/rfc/rfc2445.txt) provides an
interoperable calendar format.  This library implements functions to
read and write them using OCaml.

At the moment, the interface is minimal.  Please contribute!

## How to build manually?

You need to install menhir and ocamllex.

* To compile files, use
```
make
```

* To install with ocamlfind, use
```
make install
```
